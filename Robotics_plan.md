**Voice Controlled Robot**
=======


*Overview*
-----------




The project involves making a voice-controlled robot, which understands specific commands and acts accordingly. The robot will be 2WD, it will operate indoors and has the ability to move around autonomously until given further instructions. The main problems with this project will be voice recognition and autonomous navigation in a room. 
Voice recognition may become an issue, since we as humans have to give output, which the machine has to recognize as a command and act accordingly once hearing it. In case of background noise or poor pronunciation, the robot may not hear the voice command or misunderstand it and not operate as the operator might have intended it to. In addition, if false or unknown voice command is given, the robot should not run into any errors and have either a way to show, that it did not understand the command or wait for a moment and then continue moving in its autonomous mode.
Autonomous mode will mostly rely on different sensors, which can help the robot to avoid hitting obstacles, such as walls and other robots and humans. The main problem with independent driving is getting the correct readings and making it so, that the machine can get itself out of dead-ends and does not get itself stuck into a loop of move forward - move backwards, when it drives into a tight spot.






________________


*Schedule for pair A*
-----------
The list follows the work order and shows deadlines. As we are not experienced enough to give a proper evaluation of how many hours most of these points might take, we�ve added deadlines instead by which time the task should be completed.
1. Find different ways how to make an autonomous mode for the project and find a way to make the robot act upon instructions + how many commands will be included - due 11.11.18
2.Create the first version of autonomous mode, which can avoid bigger obstacles -  due 15.11.18
3.Create the first version of acting upon instructions, which can act upon the more basic commands - due 18.11.18
4.Combining the work of pair A and pair B and filming the demo video + analyzing and finding solutions to problems in the project - due 26.11.18
5.Work on fixing and improving the project - due 09.12.18
6.Making the poster of the project - due 12.12.18
7.Final improvements and modifications if needed - due 16.12.18
8.Poster session with live demo - due 17.12.18






*Schedule for pair B*
-----------
1.Week 10: Find out different options for voice recognition on Raspberry pi
2.Week 11: Get hardware, and start working on the device acting upon getting instructions (voice controls)
3.Week 12: Functioning voice recognition is finished, which understands basic movement commands. Then begins the combining of software of both teams
4.Week 13: Demo ready
5.Week 14: Poster
7.Week 15: Final improvements and modifications if required
8.Week 16: Final showcase


*Component list*
-----------
| Items                         | Quantity  |    You-need-this-component-from-us      |
|-------------------------------|-----------|-----------------------------------------|
| SD-card                       | 1         | Yes                                     |
| Raspberry Pi                  | 1         | Yes                                     |
| GoPiGo                        | 1         | Yes                                     |
| Microphone                    | 1         | Would be great                          |
| Voice recognition software    | 1         | No                                      |
| Ultrasonic sensor             | 1         | Yes                                     |
| LiPo battery                  | 1         | Yes                                     |
| Jumper wires                  | 8         | Yes                                     |
| Arduino Nano                  | 1         | Yes                                     |
| Mini USB cable for Nano       | 1         | Yes                                     |
| Breadboard                    | 1         | Yes                                     |
| Piezo Buzzer or similar       | 1         | If there are any                        |
| Jumper Wires(M/F)             | 4         | Yes                                     |
| Jumper Wires(F/F)             | 2         | Yes                                     |
| Jumper Wires(M/M)             | 2         | Yes                                     |