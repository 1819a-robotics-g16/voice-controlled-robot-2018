#!/usr/bin/env python3
# coding=utf-8
import serial
import gopigo as go
import time
from gopigo import *
import json
import os
import time
import numpy as np
import _thread

#img_orig = cv2.imread('map.png')
go.set_speed(50)
us_pos = 0
ser = serial.Serial('/dev/ttyUSB0', 9600)



def otse():
    go.fwd()
    
def tagasi():
    go.bwd()
    time.sleep(1)
    

def vasakule():
    left_rot()

#lineSensorOffset = 0'
def autonoomsus():
    global ser
    try:
        #go.set_speed(60)
        dist = -1

        # Make sure arduino is ready to send the data.
        print("Syncing serial...0%\r", end='')
        while ser.in_waiting == 0:
            ser.write("R".encode())
        print("Syncing serial...50%\r", end='')
        while ser.in_waiting > 0:
            ser.readline()
        print("Syncing serial...100%")


        '''
        This is the main thread, which should be running more important code such as
        Getting the sensor info from the serial and driving the robot.
        '''
        while True:

            # Read the serial input to string
            ser.write("R".encode()) # Send something to the Arduino to indicate we're ready to get some data.
            serial_line = ser.readline().strip() # Read the sent data from serial.

            try:

                # Decode the received JSON data
                data = json.loads(serial_line.decode())
                # Extract the sensor values
                
                dist = data['us1']
            except Exception as e:  # Something went wrong extracting the JSON.
                dist = -1           # Handle the situation.
                print(e)
                pass


            if dist != -1: # If a JSON was correctly extracted, continue.
                # Print received to the console
                print("DIST: ", dist)
                if dist == 0 or dist > 200:
                    otse()
                elif dist < 100:
                    tagasi()
                else:
                    print("Me ei jookse ära, me taganeme")
                    vasakule()

    except KeyboardInterrupt:
        print("Serial closed, program finished")
        go.stop()
autonoomsus()

