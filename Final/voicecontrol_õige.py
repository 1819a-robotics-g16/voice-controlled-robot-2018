#!/usr/bin/env python3
#Et mikrofon töötaks, kasutatakse veel moodulit PyAudio, mida ei pea importima.
import speech_recognition as sr
import serial
import gopigo as go
import time
from gopigo import *
import json
import os
import time
import numpy as np
import _thread

go.stop()
go.set_left_speed(60)
go.set_right_speed(60)
us_pos = 0
us_pos2 = 0
ser = serial.Serial('/dev/ttyUSB0', 9600)

def otherThread():
    def otse():
        go.fwd()
    
    def tagasi():
        go.bwd()
    

    def vasakule():
        left_rot()

    #lineSensorOffset = 0'
    def autonoomsus():
        global ser
        global z, y
        try:
            #go.set_speed(60)
            dist = -1
            dist2 = -1

            # Make sure arduino is ready to send the data.
            print("Syncing serial...0%\r", end='')
            while ser.in_waiting == 0:
                ser.write("R".encode())
            print("Syncing serial...50%\r", end='')
            while ser.in_waiting > 0:
                ser.readline()
            print("Syncing serial...100%")


            '''
            This is the main thread, which should be running more important code such as
            Getting the sensor info from the serial and driving the robot.
            '''
            start = time.time()
            while True:
                end = time.time()
                if end - start > 5:
                    #print(z)
                    start = time.time()
                # Read the serial input to string
                ser.write("R".encode()) # Send something to the Arduino to indicate we're ready to get some data.
                serial_line = ser.readline().strip() # Read the sent data from serial.
                
                try:

                    # Decode the received JSON data
                    data = json.loads(serial_line.decode())
                    # Extract the sensor values
                    dist2 = data['us2']
                    dist = data['us1']
                except Exception as e:  # Something went wrong extracting the JSON.
                    dist = -1
                    dist2 = -1    # Handle the situation.
                    print(e)
                    pass
                if z == 0 and y == 0:
                    print("x", z)
                    go.stop()
                    y = 1
                    
                if dist != -1 and dist2 != -1 and z == 1: # If a JSON was correctly extracted, continue.
                    y = 0  
                    print("x", z)
                    # Print received to the console
                    #print("DIST: ", dist, "DIST2: ", dist2)
                    if dist > 300 or dist2 > 300 or dist == 0 or dist2 == 0:
                        otse()
                    elif dist < 100 or dist2 < 100:
                        tagasi()
                    else:
                        print("Me ei jookse ära, me taganeme")
                        vasakule()

        except KeyboardInterrupt:
            print("Serial closed, program finished")
            go.stop()
    while 1:        
        autonoomsus()
        

global z, y
z = 0
y = 0
#Mikrofonist saadud input

def voicedef():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source)
        print("Say something!")
        audio = r.record(source, duration = 5)
    try:
        voice = r.recognize_google(audio)
        movement(voice)
    except sr.UnknownValueError:
        print("Google Speech Recognition ei tuvastanud ühtegi sõna.")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))
    voicedef()

def movement(voice):
    global x, y, z
    z = 0
    bias1 = enc_read(0) #initial encoder values of wheels
    bias2 = enc_read(1) #initial encoder values of wheels
    
    listInt = []
    
    listVoice = voice.split()
    print(listVoice)
    for x in listVoice:
        try:
            listVoice.split("°")
        except:
            pass
        try:
            if (int(x)) % 1 == 0:
                listInt.append(x)
        except ValueError:
            pass
        
    if "autonomic" in listVoice:
        z = 1
        print("xindef", z)
    if len(listInt) > 0:
        nr = listInt[0]
        while 1:
            print(enc_read(0), enc_read(1))
            ENC1_MOVED_TICKS = enc_read(0) # read the encoder of the left wheel
            ENC2_MOVED_TICKS = enc_read(1)
            enc_pos = int((ENC1_MOVED_TICKS - bias1) * 11)
            
            if "forward" in listVoice or "go" in listVoice:
                if "centimeters" in listVoice or "cm" in listVoice or "centimeter" in listVoice:
                    go.forward()
                    print("ok")
                    if (int(nr)*10) < enc_pos:
                        go.stop()
                        break
                if "meter" in listVoice or "meters" in listVoice:
                    go.forward()
                    print("ok")
                    if (int(nr)*1000) < enc_pos:
                        go.stop()
                        break
                if "meter" not in listVoice and "centimeter" not in listVoice and "meters" not in listVoice and "centimeters" not in listVoice and "cm" not in listVoice:
                    print("else1")
                    break
            if "backward" in listVoice or "backwards" in listVoice:
                if "centimeters" in listVoice:
                    go.backward()
                    print("ok")
                    if (int(nr)*10) < enc_pos:
                        go.stop()
                        break
                if "meter" in listVoice or "meters" in listVoice:
                    go.backward()
                    print("ok")
                    if (int(nr)*1000) < enc_pos:
                        go.stop()
                        break
                if "meter" not in listVoice and "meters" not in listVoice and "centimeters" not in listVoice and "cm" not in listVoice:
                    go.backward()
                    print("ok")
                    if (int(nr)*1000) < enc_pos:
                        go.stop()
                        break
            if "left" in listVoice:
                go.left_rot()
                if (int(nr)/2) < (enc_pos*(1)):
                    go.stop()
                    break
                    
            if "right" in listVoice:
                go.right_rot()
                if (int(nr)/2) < enc_pos*(1):
                    go.stop()
                    break
            if "backward" not in listVoice and "forward" not in listVoice and "backwards" not in listVoice and "right" not in listVoice and "left" not in listVoice:
                print("else2")
                break
            

            

    if voice == "forward":
        go.forward()
        print("ok")
        time.sleep(3)
        go.stop()
    elif voice == "left":
        go.left()
        print("ok")
        time.sleep(1)
        go.stop()
    elif voice == "right":
        go.right()
        print("ok")
        time.sleep(1)
        go.stop()
    elif voice == "backwards":
        go.backward()
        print("ok")
        time.sleep(3)
        go.stop()
    elif voice == "stop":
        go.stop()
        print("ok")
        go.stop()
    return
_thread.start_new_thread(otherThread, ())
voicedef()
        
