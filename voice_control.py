#!/usr/bin/env python3
#Et mikrofon töötaks, kasutatakse veel moodulit PyAudio, mida ei pea importima.
import speech_recognition as sr
import gopigo as go

#Mikrofonist saadud input
r = sr.Recognizer()
with sr.Microphone() as source:
    print("Say something!")
    audio = r.listen(source)

try:
    voice = r.recognize_google(audio)
    print(voice)
except sr.UnknownValueError:
    print("Google Speech Recognition ei tuvastanud ühtegi sõna.")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))
    
if voice == "forward":
    go.forward()
    print("ok")
elif voice == "left":
    go.left()
    print("ok")
elif voice == "right":
    go.right()
    print("ok")
elif voice == "backward":
    go.backward()
    print("ok")
elif voice == "stop":
    go.stop()
    print("ok")
