from gopigo import *  #v import gopigo as go ja siis kõik move commandid ümber teha?
import time
import random



minimaalne = 100
go.set_speed(50) #ohutuse mõttes alguseks, pärast annab muuta

def self_aware():
    lahkun = True
    while lahkun:
        servo_pos = 70
        time.sleep(1)
        vahemaa = us_dist(15)  #sulgudes supersonicu pini nr
        if vahemaa > minimaalne:
            print('Takistused puuduvad', vahemaa)
            fwd()
            time.sleep(1)
        else:
            print('Takistus', vahemaa)
            stop()
            servo_pos = 28
            time.sleep(1)
            vasak = us_dist(15)
            time.sleep(1)
            servo_pos = 112
            parem = us_dist(15)
            time.sleep(1)

            if vasak > parem and vasak > minimaalne:
                left()
                time.sleep(1)
            elif vasak < parem and parem > minimaalne:          
                right()
                time.sleep(1)
            else:
                bwd()
                time.sleep(2)
                keerlemisvalikud = [right_rot, left_rot]
                keeramine = keerlemisvalikud[random.randrange(0,2)]
                keeramine()
                time.sleep(1)

            stop()
                
stop()
enable_servo()
servo(70)
time.sleep(3)
self_aware()